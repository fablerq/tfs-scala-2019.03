package tfs.naval.field

import tfs.naval.model.{Field, Ship}

trait FieldController {

  // определить, можно ли его поставить
  def validatePosition(ship: Ship, field: Field): Boolean

  // добавить корабль на карту
  def markUsedCells(field: Field, ship: Ship): Field

}
