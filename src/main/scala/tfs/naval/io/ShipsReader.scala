package tfs.naval.io

import tfs.naval.model.Ship

trait ShipsReader {

  def read: Vector[(String, Ship)]

}
